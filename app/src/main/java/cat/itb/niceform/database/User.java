package cat.itb.niceform.database;

import android.provider.ContactsContract;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import java.sql.Timestamp;
import java.util.Date;

@Entity(tableName = "User")

public class User {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_puntuacio")
    private int idUser;
    private String userName;
    private String password;
    private String email;
    private String name;
    private String surnames;
    private String birthDate;
    private String gender;

    public User(String userName, String password, String email, String name, String surnames, String birthDate, String gender) {
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.name = name;
        this.surnames = surnames;
        this.birthDate = birthDate;
        this.gender = gender;
    }


    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
