package cat.itb.niceform.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import cat.itb.niceform.database.User;

@Dao
public interface UserDao {

    @Insert
    void insert(User u);

    @Update
    void update(User u);

    @Query("SELECT * FROM User WHERE userName = :userName")
    User getUser(String userName);

    @Query("SELECT * FROM User")
    List<User> getAllUser();

    @Query("SELECT password FROM User WHERE userName = :user ")
    String getpassword(String user);
}
