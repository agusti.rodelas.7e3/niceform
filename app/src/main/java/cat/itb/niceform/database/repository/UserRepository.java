package cat.itb.niceform.database.repository;

import java.util.List;

import cat.itb.niceform.database.User;
import cat.itb.niceform.database.dao.UserDao;

public class UserRepository {

    UserDao dao;

    public UserRepository(UserDao dao) {
        this.dao = dao;
    }

    public void insert(User u){ dao.insert(u);}

    public void update(User u){
        dao.update(u);
    }

    public List<User> getAllUser(){
        return dao.getAllUser();
    }

    public User getUser(String userName){
        return dao.getUser(userName);
    }
}
