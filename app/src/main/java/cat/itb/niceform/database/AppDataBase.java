package cat.itb.niceform.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import cat.itb.niceform.database.dao.UserDao;

@Database(entities = {User.class}, version = 2)
public abstract class AppDataBase extends RoomDatabase {

    public static AppDataBase INSTANCE;

    public abstract UserDao userDao();

    public static AppDataBase getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context, AppDataBase.class, "Database.db")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }
}
