package cat.itb.niceform;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputLayout;

import cat.itb.niceform.database.User;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    AutoCompleteTextView menuGenderPronoun;
    TextInputLayout editTextUsername, editTextPassword, editTextRePassword, editTextEmail, editTextName, editTextSurnames, editTextBirthDate, menuPronounGenderLayout;
    Button login, register;

    DataBaseCheck dataBaseCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        menuGenderPronoun = findViewById(R.id.autoCompleteTextViewGender);
        editTextUsername = findViewById(R.id.userNameFieldReg);
        editTextPassword = findViewById(R.id.passwordNameFieldReg);
        editTextRePassword = findViewById(R.id.repeatPasswordNameField);
        editTextEmail =  findViewById(R.id.emailNameField);
        editTextName = findViewById(R.id.nameNameField);
        editTextSurnames = findViewById(R.id.surnameNameField);
        editTextBirthDate = findViewById(R.id.dataNameField);
        menuPronounGenderLayout = findViewById(R.id.menuPronounGender);

        login = findViewById(R.id.buttonLoginRegPage);
        register = findViewById(R.id.buttonRegisterRegPage);

        dataBaseCheck = new DataBaseCheck(getApplicationContext());
        editTextBirthDate.getEditText().setInputType(InputType.TYPE_NULL);

        //Inicialitzacio DatePicker
        MaterialDatePicker.Builder materialDateBuilder = MaterialDatePicker.Builder.datePicker();
        materialDateBuilder.setTitleText("SELECT A DATE");
        final MaterialDatePicker materialDatePicker = materialDateBuilder.build();

        String[] items = {"Famale", "Male", "Other"}; //opcions del menu
        //Adapter i implementació
        ArrayAdapter<String> adapter =  new ArrayAdapter<String>(getApplicationContext(), R.layout.list_item, items);
        menuGenderPronoun.setAdapter(adapter);

        //Mostra el fragment del datePicker si es toca la icona
        editTextBirthDate.setEndIconOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialDatePicker.show(getSupportFragmentManager(), "MATERIAL_DATE_PICKER");
            }
        });

        //Mostra el fragment del datePicker
        editTextBirthDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialDatePicker.show(getSupportFragmentManager(), "MATERIAL_DATE_PICKER");
            }
        });

        //Acció al donar-li ok al DatePicker
        materialDatePicker.addOnPositiveButtonClickListener(
                new MaterialPickerOnPositiveButtonClickListener() {
                    @Override
                    public void onPositiveButtonClick(Object selection) {
                        editTextBirthDate.getEditText().setText( materialDatePicker.getHeaderText());
                    }
                });

        register.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonRegisterRegPage:
                if(verifyForm()){
                    dataBaseCheck.addUser(new User(editTextUsername.getEditText().getText().toString(),
                            editTextPassword.getEditText().getText().toString(),
                            editTextEmail.getEditText().getText().toString(),
                            editTextName.getEditText().getText().toString(),
                            editTextSurnames.getEditText().getText().toString(),
                            editTextBirthDate.getEditText().getText().toString(),
                            menuGenderPronoun.getText().toString()));
                    Intent messege = new Intent(getApplicationContext(), MessageActivity.class);
                    messege.putExtra("UserName",editTextUsername.getEditText().getText().toString());
                    startActivity(messege);
                }
                break;
            case R.id.buttonLoginRegPage:
                Intent login = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(login);
                break;
        }
    }

    public boolean verifyForm(){
        String user = editTextUsername.getEditText().getText().toString();
        String password = editTextPassword.getEditText().getText().toString();
        String rePassword = editTextRePassword.getEditText().getText().toString();
        String email = editTextEmail.getEditText().getText().toString();
        String name = editTextName.getEditText().getText().toString();
        String surnames = editTextSurnames.getEditText().getText().toString();
        String birthDate = editTextBirthDate.getEditText().getText().toString();
        String gender = menuGenderPronoun.getText().toString();

        boolean nice = true;

        if(user.isEmpty()){
            editTextUsername.setError("Please do not leave the field empty");
            nice = false;
        }else if (dataBaseCheck.UserNameExist(user)){
            editTextUsername.setError("Username is already exists, try with other one");
            nice = false;
        }

        if(password.isEmpty()){
            editTextPassword.setError("Please do not leave the field empty");
            nice = false;
        }else if(password.length() < 8) {
            editTextPassword.setError("Password must have 8 or more characters");
            nice = false;
        }

        if (!rePassword.equals(password)){
            editTextRePassword.setError("You have to repeat the password");
            nice = false;
        }

        if(email.isEmpty()){
            editTextEmail.setError("Please do not leave the field empty");
            nice = false;
        }

        if(name.isEmpty()){
            editTextName.setError("Please do not leave the field empty");
            nice = false;
        }

        if(surnames.isEmpty()){
            editTextSurnames.setError("Please do not leave the field empty");
            nice = false;
        }

        if(birthDate.isEmpty()){
            editTextBirthDate.setError("Please do not leave the field empty");
            nice = false;
        }

        if(gender.isEmpty()){
            menuPronounGenderLayout.setError("Please do not leave the field empty");
        }

        return nice;

    }
}