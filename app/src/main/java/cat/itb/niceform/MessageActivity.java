package cat.itb.niceform;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import cat.itb.niceform.database.User;

public class MessageActivity extends AppCompatActivity {

    TextView message;
    DataBaseCheck dbc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        message = findViewById(R.id.textViewMessage);

        dbc = new DataBaseCheck(getApplicationContext());

        final Bundle bundle = getIntent().getExtras();

        message.setText(getMessage(bundle.getString("UserName")));

    }

    private String getMessage(String userName){
        User user = dbc.getUser(userName);
        String pronoun;

        if(user.getGender().equals("Male")){
            pronoun = "Mr";
        }else if(user.getGender().equals("Famale")){
            pronoun = "Mrs";
        }else {
            pronoun = "";
        }

        return "Hello "+pronoun+" "+user.getName()+" "+user.getSurnames()+" with birth date "+ user.getBirthDate()+" and Email "+user.getEmail();
    }
}