package cat.itb.niceform;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    DataBaseCheck dbc;

    TextInputLayout inputUserName, inputPassword;
    Button login, register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inputUserName = findViewById(R.id.userNameField);
        inputPassword = findViewById(R.id.passwordNameField);

        login = findViewById(R.id.buttonLoginLoginPage);
        register = findViewById(R.id.buttonRegisterLoginPage);

        dbc = new DataBaseCheck(getApplicationContext());

        login.setOnClickListener(this);
        register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonLoginLoginPage:
                if(verify()){
                    Intent messege = new Intent(getApplicationContext(), MessageActivity.class);
                    messege.putExtra("UserName",inputUserName.getEditText().getText().toString());
                    startActivity(messege);
                }
                break;

        }

    }

    private boolean verify(){
        String userName = inputUserName.getEditText().getText().toString();
        String password = inputPassword.getEditText().getText().toString();

        boolean nice = true;

        if(userName.isEmpty()){
            inputUserName.setError("Please do not leave the field empty");
            nice = false;
        }else if(!dbc.UserNameExist(userName)){
            inputUserName.setError("User not exists");
            nice = false;
        }

        if (password.isEmpty()){
            inputPassword.setError("Please do not leave the field empty");
            nice = false;
        }else if(!password.equals(dbc.getPassword(userName))){
            inputPassword.setError("Password incorrect");
            nice = false;
        }

        return nice;
    }
}