package cat.itb.niceform;

import android.content.Context;

import cat.itb.niceform.database.AppDataBase;
import cat.itb.niceform.database.User;
import cat.itb.niceform.database.dao.UserDao;
import cat.itb.niceform.database.repository.UserRepository;

public class DataBaseCheck {

    static AppDataBase db;
    static UserDao userDao;
    static UserRepository userRepository;

    private Context context;

    public DataBaseCheck(Context context) {
        this.context = context;

        db = AppDataBase.getInstance(context);
        userDao = db.userDao();
        userRepository = new UserRepository(userDao);
    }

    public boolean UserNameExist(String userName){

        if(userRepository.getUser(userName) == null){
            return false;
        }else {
            return true;
        }

    }

    public void addUser(User u){
        userRepository.insert(u);
    }

    public User getUser(String userName){
        return userRepository.getUser(userName);
    }

    public String getPassword(String userName){
        return userRepository.getUser(userName).getPassword();
    }


}
